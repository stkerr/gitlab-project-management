import os
import base64
import datetime
from dotenv import load_dotenv
import json

load_dotenv()

GITLAB_API_TOKEN = os.getenv('GITLAB_API_TOKEN')
from python_graphql_client import GraphqlClient

# Instantiate the client with an endpoint.
client = GraphqlClient(endpoint="https://gitlab.com/api/graphql")

def get_issues(query):

    import collections
    issues = collections.defaultdict(list)
    issues = []

    startCursor = None
    while True:
        variables = {"cursor": startCursor}

        # Synchronous request
        data = client.execute(query=query, variables=variables)
        #data = json.loads(json.dumps(data))
        print('---')
        print(data)
        data = data['data']
        print(data)  # => {'data': {'country': {'code': 'CA', 'name': 'Canada'}}}

        if data['project']['issues']['pageInfo']['startCursor'] == None:
            break

        startCursor = data['project']['issues']['pageInfo']['startCursor']
        print(startCursor)
        decoded_start = base64.b64decode(startCursor + '='*(4-len(startCursor)%4))

        endCursor = data['project']['issues']['pageInfo']['endCursor']
        decoded_end = base64.b64decode(endCursor + '='*(4-len(endCursor)%4))

        startCursor = endCursor

        tmp_issues = data['project']['issues']['edges']
        for i in tmp_issues:
            labels = i['node']['labels']['edges']
            workflow = 'empty'
            nextUp = False
            for l in labels:
                l = l['node']['title']
                print(l)
                if 'Next Up' in l:
                    nextUp = True
                if 'workflow::' in l:
                    workflow = l
                    break
            issues.append({
            #issues[workflow].append({
                'workflow': workflow,
                'webUrl': i['node']['webUrl'],
                'weight': i['node']['weight'],
                'age': (datetime.datetime.now() - datetime.datetime.fromisoformat(i['node']['createdAt'].replace('Z',''))).days,
                'nextUp': nextUp
            })
        print(tmp_issues)
    return issues

# Create the query string and variables required for the request.
query = """
query ($cursor: String) {
  project(fullPath:"gitlab-org/gitlab") {
    issues(labelName:"group::compliance", state:opened, after:$cursor) {
    pageInfo {
        endCursor
        startCursor
      },
      edges {
        node {
          webUrl,
          createdAt,
          labels {
            edges {
              node {
                title
              }
            }
          }
        }
      }
    }
  }
}
"""

# Create the query string and variables required for the request.
scheduling_weight_query = """
query ($cursor: String) {
  project(fullPath:"gitlab-org/gitlab") {
    issues(labelName:["group::compliance", "workflow::scheduling"], state:opened, after:$cursor) {
    pageInfo {
        endCursor
        startCursor
      },
      edges {
        node {
          webUrl,
          createdAt,
          weight,
          labels {
            edges {
              node {
                title
              }
            }
          }
        }
      }
    }
  }
}
"""

ready_for_dev_query = """
query ($cursor: String) {
  project(fullPath:"gitlab-org/gitlab") {
    issues(labelName:["group::compliance", "workflow::ready for development"], state:opened, after:$cursor) {
    pageInfo {
        endCursor
        startCursor
      },
      edges {
        node {
          webUrl,
          createdAt,
          weight,
          labels {
            edges {
              node {
                title
              }
            }
          }
        }
      }
    }
  }
}
"""

def write_to_csv(filename, issues):
    import csv
    with open(filename, "w", newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(issues[0].keys())
        print(issues[0])
        for i in issues:
            print(i)
            print(i.values())
            writer.writerow(i.values())

scheduling_issues = get_issues(scheduling_weight_query)
ready_for_dev_issues = get_issues(ready_for_dev_query)

write_to_csv("weighted.csv", scheduling_issues + ready_for_dev_issues)

#issues = get_issues(query)
#print(json.dumps(issues))

